# ifpadmin CHANGELOG

## [0.2.0] - 2022-08-16

### Added

- Add migrations for employees, countries, designations

## [0.1.0] - 2022-08-16

### Added

- Initial commit
