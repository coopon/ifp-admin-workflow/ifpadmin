# syntax=docker/dockerfile:1

FROM clojure:openjdk-18-lein-slim-bullseye
WORKDIR /app
COPY ./ ./
RUN lein uberjar
RUN lein migrate
CMD java -jar target/ifpadmin-standalone-release.jar
