(ns ifpadmin.auth.users.db
  (:require [hugsql.core :as hugsql]))

(hugsql/def-db-fns "ifpadmin/auth/users/users.sql")
