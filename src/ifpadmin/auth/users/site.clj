(ns ifpadmin.auth.users.site
    (:require [ring.util.response :as ring-resp]
              [ifpadmin.site :as site]
              [ifpadmin.auth :as auth]
              [ifpadmin.auth.users.forms :as forms]
              [ifpadmin.auth.users :as auth.users]
              [buddy.auth :as ba]))

(defn login-page [request]
  (ring-resp/response
   (site/base
    "Login"
    (forms/login-control request))))

(defn login-post [request]
  (let [data (:form-params request)
        user (auth/authenticate-username-password data)
        session (:session request)]
    (if (nil? user)
      (ring-resp/response "Username or password is incorrect")
      (let [updated-session (assoc session
                                   :identity (keyword (:username user))
                                   :role (keyword (:role user)))]
        (assoc (ring-resp/redirect "/")
               :session updated-session)))))

(defn logout
  "Logs out the authenticated session"
  [request]
  (assoc (ring-resp/redirect "/")
         :session {}))

(defn register-page [request]
  (ring-resp/response
   (site/base
    "Register"
    (forms/register-control request))))

(defn register-post [request]
  (let [data (:form-params request)
        session (:session request)]
    (if (= (:password data) (:confirm-password data))
      (if (auth.users/user-exists? data)
        (do (auth.users/new-user data)
            (let [updated-session (assoc session
                                         :identity (keyword (:username data))
                                         :role :user)]
              (assoc (ring-resp/redirect "/")
                     :session updated-session)))
        (ring-resp/response "Username or email already taken"))
      (ring-resp/response "Password doesn't match"))))
