(ns ifpadmin.auth.roles.db
  (:require [hugsql.core :as hugsql]))

(hugsql/def-db-fns "ifpadmin/auth/roles/roles.sql")
