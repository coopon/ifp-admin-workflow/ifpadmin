(ns ifpadmin.site.home
  (:require [hiccup.core :as hiccup]
            [ring.util.response :as ring-resp]
            [ifpadmin.site :as site]
            ))

(defn home-page [request]
  (ring-resp/response "Hello World!"))
